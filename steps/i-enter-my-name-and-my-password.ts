import { t } from 'testcafe';
import { IConfig } from '../config/config.interface';
import { getCurrentConfig } from '../config/testcafe-config';
import { IPageModel } from '../models';
import * as selector from '../selectors';

/**
 * @step
 * @given,@when("I enter my name and my password")
 */
export default async (_: string) => {
  // get the config that was injected into the fixture/test context by the feature
  const config: IConfig = getCurrentConfig(t);

  // get the page object model that was injected in the context
  const inputData = t.ctx.inputData as IPageModel;

  const name = inputData.name || '';
  const password = inputData.password || '';

  await t
    .setTestSpeed(config.testSpeed)
    .typeText(selector.lkdLoginName, name, { replace: true })
    .typeText(selector.lkdLoginPassword, password, { replace: true })
    .click(selector.lkdLoginIndentifyButton)
    .expect(selector.lkdMyprofileName.exists).ok()
};

import { t } from 'testcafe';
import { IConfig } from '../config/config.interface';
import { getCurrentConfig } from '../config/testcafe-config';
import { IPageModel } from '../models';
import * as selector from '../selectors';

/**
 * @step
 * @when("I search a job in rennes")
 */
export default async (stepName: string) => {
  // get the config that was injected into the fixture context by the feature
  const config: IConfig = getCurrentConfig(t);

  // get the page object model that was injected in the test context
  const inputData = t.ctx.inputData as IPageModel;

  const job = inputData.job || '';
  const city = inputData.city || '';

  await t
    .setTestSpeed(config.testSpeed)
    .typeText(selector.lkdJob, job, { replace: true })
    .typeText(selector.lkdCity, city, { replace: true })
    .click(selector.lkdSearchJob)
};

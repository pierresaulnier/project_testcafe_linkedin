import { t } from 'testcafe';
import { IConfig } from '../config/config.interface';
import { getCurrentConfig } from '../config/testcafe-config';
import { IPageModel } from '../models';
import * as selector from '../selectors';

/**
 * @step
 * @given,@then("I acces on my page linkedin")
 */
export default async (_: string) => {
  // get the config that was injected into the fixture/test context by the feature
  const config: IConfig = getCurrentConfig(t);

  // get the page object model that was injected in the context
  const inputData = t.ctx.inputData as IPageModel;

  const profileName = inputData.profileName || '';

  await t
    .setTestSpeed(config.testSpeed)
    .expect(selector.lkdMyprofileName.exists).ok()
    .expect(selector.lkdMyprofileName.textContent).contains(profileName)
};

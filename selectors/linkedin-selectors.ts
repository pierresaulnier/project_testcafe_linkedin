import { Selector } from 'testcafe';
export const lkdLoginButton = Selector('a').withAttribute('class', 'nav__button-secondary');
export const lkdLoginName = Selector('input').withAttribute('id', 'username');
export const lkdLoginPassword = Selector('input').withAttribute('id', 'password');
export const lkdLoginIndentifyButton = Selector('button').withAttribute('type', 'submit');
export const lkdMyprofileName = Selector('span').withAttribute('class', 't-16 t-black t-bold');
export const jobIcon = Selector('span').withAttribute('id', 'jobs-tab-icon');
export const careersBlock = Selector('div').withAttribute('id', 'careers');
export const lkdJob = Selector('input').withAttribute('aria-label', "Recherche d’emplois");
export const lkdCity = Selector('input').withAttribute('aria-label', "Recherche du lieu");
export const lkdSearchJob = Selector('button').withText('Recherche');
export const lkdJobsFind = Selector('div').withAttribute('class', 't-12 t-black--light t-normal');

/* to kill */
export const firstInputBox = Selector('input[type=text]').nth(0);
export const secondInputBox = Selector('input[type=text]').nth(1);
export const userNameInputBox = Selector('input#developer-name[type=text]');
export const submitButton = Selector('button[type=submit]');
export const resultContent = Selector('div.result-content');
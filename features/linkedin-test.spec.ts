import 'testcafe';
import { getCurrentConfig } from '../config/testcafe-config';
import { pageModel } from '../models';
import { env } from '../step-filters/env';
import { and, given, then, when } from '../step-runner';

/**
 * @feature
 */
fixture('Feature: linkedin')
  .before(async (ctx) => {
    // inject global configuration in the fixture context
    ctx.config = getCurrentConfig();
  })
  .beforeEach(async (t) => {
    // inject page model in the test context
    t.ctx.inputData = pageModel;
  });


// test('Scenario: login is possible', async () => {
//   await given('I navigate to linkedin');
//   await then('I can see login button');
//   await when('I enter my name and my password');
//   await then('I acces on my page linkedin');
// });


test('Scenario: check jobs', async () => {
  await given('I navigate to linkedin');
  await then('I can see login button');
  await when('I enter my name and my password');
  await then('I acces on my page linkedin');
  await when('I click on job icon');
  await then('I acces on job page');
  await when('I search a job in rennes');
  await then('I find some jobs');
});


/*
test('Scenario: send feedback', async () => {
  await env.only('devci');
  await given('I enter my name');
  await when('I send my feedback on testcafe');
  await then("a 'Thank you' message should appear with my name");
})
*/
;
